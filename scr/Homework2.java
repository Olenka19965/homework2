package scr;
import java.util.Scanner;
public class Homework2 {
    public static void main(String[] args) {
        char [][] board = new char[5][5];
        for (int i = 0; i < board[i].length; i++) {
            for (int j = 0; j < board[j].length; j++){
                board[i][j]='.';

            }
        }
        System.out.println("Гра почалась!");
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.print("Рядок (1-5): ");
            int row = scanner.nextInt();
            System.out.print("Стовпець (1-5): ");
            int colum = scanner.nextInt();
            if (row == 3 && colum == 3){
                board[row-1][colum-1]='x';
                printBoard(board);
            }
        }
    }
    public static void printBoard(char[][] board){
        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board.length; j++){
                System.out.print(board[i][j]+" ");
            }
            System.out.println();
        }

    }

}
